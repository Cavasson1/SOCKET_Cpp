#include <stdio.h>
#include <winsock.h> // Inclui o header do winsock.
#include <conio.h>
#include <string.h>
#include <stdlib.h>

#include "Mensagem.h"

// Esta � uma alternativa para especificar a biblioteca no campo de depend�ncias externas nas configura��es do projeto.
#pragma comment(lib, "ws2_32.lib")

// Vari�veis que recebem o valor da inicializa��o e cria��o do socket.
WSADATA data;
SOCKET winsock;

SOCKADDR_IN sock; // Struct -> cont�m a configura��o do socket a ser usado o ip, a porta e a fam�lia do socket.

void enviar(Mensagem &msg) {

	char mensagem[1024];
	msg.getMsg(mensagem);

	// Enviando dados:
	// send(SOCKET,BUFFER,TAMANHO,FLAGS);
	// SOCKET->nosso socket que foi criado;
	// BUFFER->dados a enviar;
	// TAMANHO->tamanho dos dados;
	// FLAGS -> como a fun��o se comporta. Dificilmente ser� utilizado um valor diferente de zero
	send(winsock, mensagem, msg.getTamanho(), 0);
}

int main() {

	// Inicializa o uso do winsock. Retorna o valor 0 (zero)quando � finalizada com sucesso ou - 1, quando um erro ocorre.
	// Sintaxe : WSAStartup(VERSAO_DO_WINSOCK, ENDERE�O DA VARI�VEL)
	if (WSAStartup(MAKEWORD(1, 1), &data) == SOCKET_ERROR) {
		printf("ERRO - Falha ao iniciar o Socket");
		system("pause");
		return 0;
	}

	// Inicializa o socket. Retorna os valores 0 (Socket inicializado) ou 1 (n�o inicializado).
	// Sintaxe : socket(FAMILIA, TIPO_DE_PROTOCOLO, 0); 
	// Para FAMILIA, temos diversos tipos, utilizaremos o AF_INET.
	// Tipo de protocolo : SOCK_STREAM-> protocolo TCP
	if ((winsock = socket(AF_INET, SOCK_STREAM, 0)) == SOCKET_ERROR) {
		printf("ERRO - Falha ao criar o Socket");
		system("pause");
		return 0;
	}
	printf("\nSocket Iniciado com sucesso\n");

	sock.sin_family = AF_INET;
	sock.sin_port = htons(7008);
	sock.sin_addr.s_addr = inet_addr("127.0.0.1"); // Indica o IP o qual queremos conectar.

	// Conecta o socket. Temos os valores 0(sucesso)ou - 1(falha) como retorno da fun��o connect().
	if (connect(winsock, (SOCKADDR*)&sock, sizeof(sock)) == SOCKET_ERROR) {
		printf("Erro ao se conectar");
		return 0;
	}

	printf("Conectado\n");

	Mensagem msg;
	char msgEntrada[10];

	// recebe id do cliente, e guarda no objeto msg
	int bytes = recv(winsock, msgEntrada, 10, 0);
	msg.setId(atoi(msgEntrada));

	while (1) {
		char msgSaida[1024];

		printf("Cliente[ %i ] - Digite uma mensagem:\n", msg.getId());
		scanf_s("%s", msgSaida, 1024);
		msg.setMsg(msgSaida);

		enviar(msg);

	}

	// Fica esperando at� ser precionado uma tecla do teclado e termina imediatamente
	//_getch();

	// fechar o socket
	closesocket(winsock);

	// Fechamos o nosso socket, mas ainda n�o finalizamos o uso do winsock, isto �, 
	// enquanto o programa estiver rodando, estaremos aptos a criar novos sockets. 
	// Ao finalizar um programa que utiliza sockets, utilizamos sempre WSACleanup() para encerrar o uso do winsock.
	WSACleanup();

	return 0;
}