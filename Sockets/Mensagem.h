#include <iostream>
#include <string.h>

class Mensagem {
private:
	int id;
	char msg[1024];
	int tamanho;

public:
	Mensagem();

	int getId();
	void setId(int id);

	void getMsg (char msg[]);
	void setMsg(char novaMsg[]);

	int getTamanho();
};